import src.tools
import src.compiler

import sys

"""
args:
--jsonfile:     path to .json file with code
--commandsfile: path to .txt file with commands
"""
args = src.tools.get_args(sys.argv[1:])
src.compiler.compileJsonFile(args)
