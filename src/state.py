from src.config import *
import src.handlers

class State:
    def __init__(self, stateName, handler, signals):
        """
        Class of one state
        :param stateName: Name of state
        :param handler: Handler for process input
        :param signals: All signals for that State
        """
        self.stateName = stateName
        self.handler = handler
        self.signals = signals

        if DEFAULT not in signals:
            raise KeyError(DEFAULT)
        for signalName in signals:

            if ACTION not in signals[signalName]:
                raise KeyError(ACTION)

            if NEXT not in signals[signalName]:
                raise KeyError(NEXT)


    def getsignals(self):
        """
        :return: List of all signal
        """

        return list(self.signals)

    def getaction(self, signal):
        """
        :return: an action, related to this change
        """

        return self.signals[signal][ACTION]

    def getnext(self, signal):
        """
        :return: a next state for this signal
        """

        return self.signals[signal][NEXT]

    def gethandler(self):
        """
        :return:
        """

        return self.handler


class FSM:
    def __init__(self, initialname):
        """
        Finite State Machine
        :param initialname: name of initial state
        """

        self.currentState = initialname
        self.allStates = {initialname: None}


    def makesignal(self, signal='default'):
        """
        Transition from one state to another
        :param signal: Signal for changing state
        :return: Outputs from first and second actions
        """

        currentState = self.allStates[self.currentState]
        if signal not in currentState.getsignals():
            signal = DEFAULT

        # First action
        # TODO: the dependence of signal from handler?
        fout = src.handlers.firstAction(currentState.gethandler())

        nextstate = currentState.getnext(signal)
        if nextstate not in self.allStates:
            raise ValueError(f'Error in {self.currentState}: next state "{nextstate}" doesn\'t exist')

        # Second action
        sout = src.handlers.secondAction(currentState.getaction(signal))

        # Change current state
        self.currentState = nextstate

        return (fout, sout)

    def addState(self, statename, state):
        """
        Add a new state to FSM
        :param statename: name for iterate
        :param state: an object of class State
        :return:
        """

        self.allStates[statename] = state
