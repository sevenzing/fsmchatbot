# ---------
# KEY WORDS
# ---------

INITIAL = 'initial'
STATES = 'states'
HANDLER = 'handler'
SIGNALS = 'signals'
DEFAULT = 'default'
ACTION = 'action'
NEXT = 'next'
