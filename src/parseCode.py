import json
from src.config import *

def parseJsonFile(pathToFile):
    with open(pathToFile, 'r', encoding='utf-8') as file:
        jsn = json.load(file)
    return jsn

def parseCommandsFile(pathToFile):
    with open(pathToFile, 'r', encoding='utf-8') as file:
        lst = file.read().split('\n')
    return lst

