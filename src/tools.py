import argparse


def get_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--jsonfile',
                        action='store',
                        help='Name of json file',
                        required=True)

    parser.add_argument('--commandsfile',
                        action='store',
                        help='Name of file with commands to bot',
                        required=True)

    return parser.parse_args(argv)
