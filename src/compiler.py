from src.config import *
from src.parseCode import parseCommandsFile, parseJsonFile
from src.state import State, FSM


def compileJsonFile(args):

    # Initialize
    try:
        code = parseJsonFile(args.jsonfile)
        startState = code[INITIAL]
        fsm = FSM(startState)

        if STATES not in code:
            raise KeyError(STATES)

    except KeyError as e:
        raise SyntaxError(f'Error in begin: there is not key word {e}.')

    # Creating FSM
    try:
        for stateName in code[STATES]:
            state = State(stateName,
                          code[STATES][stateName][HANDLER],
                          code[STATES][stateName][SIGNALS])

            fsm.addState(stateName, state)

    except KeyError as e:
        raise SyntaxError(f'Error in {stateName}: there is not key word {e}.')

    # Apply commands
    try:
        for command in parseCommandsFile(args.commandsfile):
            # makesignal() returns first and second handler output
            botReply = fsm.makesignal(command)
            print(f'User: {command}\nBot: {botReply[1]}')

    except FileNotFoundError:
        raise FileNotFoundError(f'file {args.commandsfile} doesn\'t exist')
